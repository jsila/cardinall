<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cardinall
 */

if ( ! is_active_sidebar( 'sidebar-footer-left' ) || ! is_active_sidebar( 'sidebar-footer-right' )) {
	return;
}
?>

<div id="footer-sidebars">
  <aside id="secondary" class="widget-area" role="complementary">
  	<?php dynamic_sidebar( 'sidebar-footer-left' ); ?>
  </aside><!-- #secondary -->

  <aside id="tertiary" class="widget-area" role="complementary">
  	<?php dynamic_sidebar( 'sidebar-footer-right' ); ?>
  </aside><!-- #secondary -->
</div>
